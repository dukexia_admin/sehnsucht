import logging

from scheduler import scheduler

logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')


@scheduler(cron='* * * * * 1,10,30,40', retry_times=5, retry_interval=3)
def task1():
    print("task 1")


@scheduler(cron='* * * * 15-40 5/10', retry_times=3, retry_interval=10)
def task2():
    print("task 2")
