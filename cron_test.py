import re
from unittest import TestCase

from scheduler import cron


class CronTest(TestCase):

    def test_re_year(self):
        re_year = "^%s$" % (cron.re_year)

        match = re.match(re_year, "2018")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("years"))
        self.assertTrue(match.group("years") == "2018")

        match = re.match(re_year, "2018/2")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("years"))
        self.assertTrue(match.group("years") == "2018/2")

        match = re.match(re_year, "2015,2016,2017,2018,2019")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("years"))
        self.assertTrue(match.group("years") == "2015,2016,2017,2018,2019")

        match = re.match(re_year, "18")
        self.assertIsNone(match)

        match = re.match(re_year, "2015-2018,2017,2018-2019")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("years"))
        self.assertTrue(match.group("years") == "2015-2018,2017,2018-2019")

        match = re.match(re_year, "2015-2018")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("years"))
        self.assertTrue(match.group("years") == "2015-2018")

        match = re.match(re_year, "2018-2022-2020,2021")
        self.assertIsNone(match)

    def test_re_month(self):
        re_month = "^%s$" % (cron.re_month)

        match = re.match(re_month, "00")
        self.assertIsNone(match)

        match = re.match(re_month, "*/2")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("months"))
        self.assertTrue(match.group("months") == "*/2")

        match = re.match(re_month, "5/1")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("months"))
        self.assertTrue(match.group("months") == "5/1")

        match = re.match(re_month, "1,3,5,7,9")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("months"))
        self.assertTrue(match.group("months") == "1,3,5,7,9")

        match = re.match(re_month, "1-5,7,9")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("months"))
        self.assertTrue(match.group("months") == "1-5,7,9")

        match = re.match(re_month, "1-5")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("months"))
        self.assertTrue(match.group("months") == "1-5")

    def test_re_day(self):
        re_day = "^%s$" % (cron.re_day)

        match = re.match(re_day, "32")
        self.assertIsNone(match)

        match = re.match(re_day, "0")
        self.assertIsNone(match)

        match = re.match(re_day, "00")
        self.assertIsNone(match)

        match = re.match(re_day, "01")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("days"))
        self.assertTrue(match.group("days") == "01")

        match = re.match(re_day, "1,3,5,6,7")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("days"))
        self.assertTrue(match.group("days") == "1,3,5,6,7")

        match = re.match(re_day, "1-7")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("days"))
        self.assertTrue(match.group("days") == "1-7")

        match = re.match(re_day, "*/7")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("days"))
        self.assertTrue(match.group("days") == "*/7")

    def test_re_hour(self):
        re_hour = "^%s$" % (cron.re_hour)

        for i in range(0, 24):
            match = re.match(re_hour, str(i))
            self.assertIsNotNone(match)

        match = re.match(re_hour, "24")
        self.assertIsNone(match)

        match = re.match(re_hour, "13/2")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("hours"))
        self.assertTrue(match.group("hours") == "13/2")

        match = re.match(re_hour, "1,2,3,4,5")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("hours"))
        self.assertTrue(match.group("hours") == "1,2,3,4,5")

        match = re.match(re_hour, "1-5")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("hours"))
        self.assertTrue(match.group("hours") == "1-5")

    def test_re_minute(self):
        re_minute = "^%s$" % (cron.re_minute)

        for i in range(0, 60):
            match = re.match(re_minute, str(i))
            self.assertIsNotNone(match)

        match = re.match(re_minute, "60")
        self.assertIsNone(match)

        match = re.match(re_minute, "37/2")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("minutes"))
        self.assertTrue(match.group("minutes") == "37/2")

        match = re.match(re_minute, "1,2,3,4,5")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("minutes"))
        self.assertTrue(match.group("minutes") == "1,2,3,4,5")

        match = re.match(re_minute, "1-5")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("minutes"))
        self.assertTrue(match.group("minutes") == "1-5")

    def test_re_second(self):
        re_second = "^%s$" % (cron.re_second)

        for i in range(0, 60):
            match = re.match(re_second, str(i))
            self.assertIsNotNone(match)

        match = re.match(re_second, "60")
        self.assertIsNone(match)

        match = re.match(re_second, "13/10")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("seconds"))
        self.assertTrue(match.group("seconds") == "13/10")

        match = re.match(re_second, "1,2,3,4,5")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("seconds"))
        self.assertTrue(match.group("seconds") == "1,2,3,4,5")

        match = re.match(re_second, "1-2,3-4,5")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("seconds"))
        self.assertTrue(match.group("seconds") == "1-2,3-4,5")

        match = re.match(re_second, "1-59")
        self.assertIsNotNone(match)
        self.assertIsNotNone(match.group("seconds"))
        self.assertTrue(match.group("seconds") == "1-59")

        match = re.match(re_second, "05")
        self.assertIsNotNone(match)

    def test_re_cron(self):
        match = re.match(cron.cron_expression, "2018 05 09 15 11 39")
        self.assertIsNotNone(match)

        match = re.match(cron.cron_expression, "2018/2 05/3 09/5 15/1 11/2 39/31")
        self.assertIsNotNone(match)

        match = re.match(cron.cron_expression, "2018,2019 05/3 09,10,11,05 15 11/2 39/31")
        self.assertIsNotNone(match)

        match = re.match(cron.cron_expression, "2018-2019 05/3 09,10,11,05 15 11/2 39/31")
        self.assertIsNotNone(match)

    def test_cron(self):
        quartz = cron.Cron("2018 05 11 11 45/3 13/19")
        next_execute_time = quartz.next_execute_time()
        self.assertTrue(str(next(next_execute_time)) == "2018-05-11 11:45:13")
        self.assertTrue(str(next(next_execute_time)) == "2018-05-11 11:45:32")
        self.assertTrue(str(next(next_execute_time)) == "2018-05-11 11:45:51")
        self.assertTrue(str(next(next_execute_time)) == "2018-05-11 11:48:13")
        self.assertTrue(str(next(next_execute_time)) == "2018-05-11 11:48:32")
        self.assertTrue(str(next(next_execute_time)) == "2018-05-11 11:48:51")

    def test_cron_scenario_1(self):
        quartz = cron.Cron("* * * 09 30 00")
        next_execute_time = quartz.next_execute_time()
        for i in range(1, 1000):
            print(next(next_execute_time))
